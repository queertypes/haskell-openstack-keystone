# Haskell Openstack Keystone Client

A Haskell binding to the Openstack
[Keystone](http://docs.openstack.org/developer/keystone/) service.

## Development

### Build

```
# initial setup
$ cabal sandbox init
$ cabal configure
$ cabal install --dependencies-only -j

# future builds
$ cabal build
```

### Testing

```
# initial setup
$ cabal configure --enable-tests
$ cabal install --dependencies-only -j

# future testing
$ cabal test
```

### Benchmarking

```
# initial setup
$ cabal configure --enable-benchmarks
$ cabal install --dependencies-only -j

# future benchmarking
$ cabal bench
```
