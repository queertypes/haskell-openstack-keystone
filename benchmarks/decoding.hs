{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.Aeson (decode)
import Data.ByteString.Lazy (ByteString)
import Criterion.Main (defaultMain, whnf, bench)

import Web.Rackspace.Identity.Common.Auth (Role)

main :: IO ()
main = defaultMain [ bench "decode Role" $ whnf (decode :: ByteString -> Maybe Role) rData ]
  where rData = "{\"description\": \"cat\", \"id\": \"1\", \"name\": \"2\"}"