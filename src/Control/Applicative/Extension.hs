module Control.Applicative.Extension (
       liftA4,
       liftA5,
       liftA6
) where

import Control.Applicative (Applicative, liftA3, (<*>))

liftA4 :: Applicative f =>
       (a -> b -> c -> d -> e) ->
       f a -> f b -> f c -> f d -> f e
liftA4 a b c d e = liftA3 a b c d <*> e

liftA5 :: Applicative f =>
       (a -> b -> c -> d -> e -> f') ->
       f a -> f b -> f c -> f d -> f e -> f f'
liftA5 a b c d e f = liftA4 a b c d e <*> f

liftA6 :: Applicative f =>
       (a -> b -> c -> d -> e -> f' -> g) ->
       f a -> f b -> f c -> f d -> f e -> f f' -> f g
liftA6 a b c d e f g = liftA5 a b c d e f <*> g