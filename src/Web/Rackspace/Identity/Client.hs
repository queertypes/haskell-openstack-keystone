{-# LANGUAGE OverloadedStrings #-}
module Web.Rackspace.Identity.Client (
  auth,

  versions,
  versionDetails,

  listUsers
) where

import Data.Monoid ((<>))
import Data.Text.Encoding (encodeUtf8)

import Control.Lens
import Data.Aeson
import Network.Wreq hiding (auth)

import Web.Rackspace.Identity.Common.Auth
import Web.Rackspace.Identity.Common.Users
import Web.Rackspace.Identity.Common.Version

-- ENDPOINTS
host, tokenUri, lUserUri :: String
host = "https://identity.api.rackspacecloud.com"
tokenUri = host <> "/v2.0/tokens"
lUserUri = host <> "/v2.0/users"

versions :: IO (Maybe VersionResponse)
versions = do
  r' <- get host
  return $ decode (r' ^. responseBody)

versionDetails :: VersionId -> IO (Maybe VersionDetailResponse)
versionDetails v = do
  let uri = host <> "/" <> show v
  r' <- get uri
  return $ decode (r' ^. responseBody)

auth :: AuthRequest -> IO (Maybe AuthResponse)
auth r = do
  r' <- post tokenUri (toJSON r)
  print (r' ^. responseBody)
  return $ decode (r' ^. responseBody)

listUsers :: Token -> IO (Maybe LUserResponse)
listUsers (Token _ (TokenId t)) = do
  let opts = defaults & header "X-Auth-Token" .~ [encodeUtf8 t]
  r' <- getWith opts lUserUri
  return $ decode (r' ^. responseBody)