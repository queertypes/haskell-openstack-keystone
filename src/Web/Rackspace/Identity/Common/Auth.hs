{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Web.Rackspace.Identity.Common.Auth (
  Username(..),
  Password(..),
  ApiKey(..),
  TenantId(..),
  User(..),

  Region(..),
  RoleDesc(..),
  RoleName(..),
  RoleId(..),
  Role(..),

  AuthRequest(..),

  PublicUrl(..),
  InternalUrl(..),
  ServiceName(..),
  ServiceType(..),
  ServiceEndpoint(..),
  Service(..),

  Expires(..),
  TokenId(..),
  Token(..),

  AuthResponse(..)
) where

import Control.Applicative
import Control.Monad (mzero)
import Data.Aeson
import Data.Time
import Data.Text

import Control.Applicative.Extension (liftA4)

newtype Username = Username Text deriving (Eq, Show, FromJSON)
newtype Password = Password Text deriving (Eq, Show)
newtype ApiKey = ApiKey Text deriving (Eq, Show)
newtype TenantId = TenantId Text deriving (Eq, Show, FromJSON)
data User = User { uRegion :: Region
                 , uTenant :: TenantId
                 , uName :: Username
                 , uRoles :: [Role]} deriving (Eq, Show)

newtype Region = Region Text deriving (Eq, Show, FromJSON)
newtype RoleDesc = RoleDesc Text deriving (Eq, Show, FromJSON)
newtype RoleName = RoleName Text deriving (Eq, Show, FromJSON)
newtype RoleId = RoleId Text deriving (Eq, Show, FromJSON)
data Role = Role {rDesc :: RoleDesc
                 , rId :: RoleId
                 , rName :: RoleName} deriving (Eq, Show)

data AuthRequest
     = PasswordAuth Username Password
     | ApiKeyAuth Username ApiKey

newtype PublicUrl = PublicUrl Text deriving (Eq, Show, FromJSON)
newtype InternalUrl = InternalUrl Text deriving (Eq, Show, FromJSON)
newtype ServiceName = ServiceName Text deriving (Eq, Show, FromJSON)
newtype ServiceType = ServiceType Text deriving (Eq, Show, FromJSON)
data ServiceEndpoint = ServiceEndpoint { sePubUrl :: PublicUrl
                                       , sePrivUrl :: Maybe InternalUrl
                                       , seRegion :: Maybe Region
                                       , seTenant :: TenantId
                                       } deriving (Eq, Show)

data Service = Service { sEndpoints :: [ServiceEndpoint]
                       , sName :: ServiceName
                       , sType :: ServiceType} deriving Show

newtype Expires = Expires UTCTime deriving (Eq, Show, FromJSON)
newtype TokenId = TokenId Text deriving (Eq, Show, FromJSON)
data Token = Token { tExpires :: Expires
                   , tTokenId :: TokenId
                   } deriving (Eq, Show)

data AuthResponse = AuthResponse { arServices :: [Service]
                                 , arToken :: Token
                                 , arUser :: User
                                 } deriving Show

instance FromJSON Role where
  parseJSON (Object v) =
    liftA3 Role (v .: "description")
                (v .: "id")
                (v .: "name")
  parseJSON _ = mzero

instance ToJSON Role where
  toJSON (Role (RoleDesc d) (RoleId i) (RoleName n)) =
    object ["description" .= d,
            "id" .= i,
            "name" .= n
           ]

instance FromJSON User where
  parseJSON (Object v) =
    liftA4 User (v .: "RAX-AUTH:defaultRegion")
                (v .: "id")
                (v .: "name")
                (v .: "roles")
  parseJSON _ = mzero

instance ToJSON AuthRequest where
  toJSON (PasswordAuth (Username u) (Password p)) =
    object ["auth" .=
            object ["passwordCredentials" .=
                    object ["username" .= u,
                            "password" .= p]
                    ]
           ]
  toJSON (ApiKeyAuth (Username u) (ApiKey k)) =
    object ["auth" .=
            object ["RAX-KSKEY:apiKeyCredentials" .=
                    object ["username" .= u,
                            "apiKey" .= k]
                    ]
            ]

instance FromJSON ServiceEndpoint where
  parseJSON (Object v) =
    liftA4 ServiceEndpoint (v .: "publicURL")
                           (v .:? "internalURL")
                           (v .:? "region")
                           (v .: "tenantId")
  parseJSON _ = mzero

instance FromJSON Service where
  parseJSON (Object v) = liftA3 Service (v .: "endpoints") 
                                        (v .: "name") 
                                        (v .: "type")
  parseJSON _ = mzero

instance FromJSON Token where
  parseJSON (Object v) = liftA2 Token (v .: "expires")
                                      (v .: "id")
  parseJSON _ = mzero

instance FromJSON AuthResponse where
  parseJSON (Object v) = do
    catalogue <- (v .: "access") >>= (.: "serviceCatalog")
    token <- (v .: "access") >>= (.: "token")
    user <- (v .: "access") >>= (.: "user")
    return $ AuthResponse catalogue token user
  parseJSON _ = mzero