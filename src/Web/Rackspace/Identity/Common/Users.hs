{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Web.Rackspace.Identity.Common.Users (
  UserId(..),
  UserEnabled(..),
  UserEmail(..),
  LUser(..),
  LUserResponse(..)
) where

import Control.Applicative ((<$>))
import Control.Monad (mzero)

import Data.Text
import Data.Aeson

import Control.Applicative.Extension (liftA4)
import Web.Rackspace.Identity.Common.Auth (Username)

newtype UserId = UserId Text deriving (Show, FromJSON)
newtype UserEnabled = Enabled Bool deriving (Show, FromJSON)
newtype UserEmail = UserEmail Text deriving (Show, FromJSON)
data LUser = LUser UserId UserEnabled Username UserEmail deriving Show

instance FromJSON LUser where
  parseJSON (Object v) =
    liftA4 LUser (v .: "id") (v .: "enabled") (v .: "username") (v .: "email")
  parseJSON _ = mzero

data LUserResponse = LUserResponse [LUser] deriving Show

instance FromJSON LUserResponse where
  parseJSON (Object v) = LUserResponse <$> (v .: "users")
  parseJSON _ = mzero
