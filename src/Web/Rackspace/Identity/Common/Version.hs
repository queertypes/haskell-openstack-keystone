{-# LANGUAGE OverloadedStrings #-}
module Web.Rackspace.Identity.Common.Version (
  VersionId(..),
  Version(..),
  VersionResponse(..),
  VersionDetailResponse(..)
) where

import Control.Applicative
import Control.Monad (mzero)
import Data.Aeson
import Data.Text

import Control.Applicative.Extension (liftA4)

newtype Href = Href Text deriving Show
newtype VersionUpdated = VersionUpdated Text deriving Show
data Rel = RelSelf | RelDescribedBy deriving Show
data Link = Link Href Rel deriving Show
data VersionId = V10 | V11 | V20
data VersionStatus = VDeprecated | VCurrent deriving Show
data Version =
  Version VersionId (Maybe [Link]) VersionStatus VersionUpdated
  deriving Show

newtype VersionResponse = VersionResponse [Version] deriving Show
newtype VersionDetailResponse = VersionDetailResponse Version deriving Show

toVersion :: Text -> VersionId
toVersion v = case v of
  "v1.0" -> V10
  "v1.1" -> V11
  "v2.0" -> V20
  _ -> error ("Unexpected version" ++ show v)

toRel :: Text -> Rel
toRel v = case v of
  "self" -> RelSelf
  "describedby" -> RelDescribedBy
  _ -> error ("unexpected rel"  ++ show v)

toVersionStatus :: Text -> VersionStatus
toVersionStatus v = case toLower v of
  "deprecated" -> VDeprecated
  "current" -> VCurrent
  _ -> error ("unexpected version status" ++ show v)

instance Show VersionId where
  show V10 = "v1.0"
  show V11 = "v1.1"
  show V20 = "v2.0"

instance FromJSON Link where
  parseJSON (Object v) =
    liftA2 Link (Href <$> v .: "href") (toRel <$> v .: "rel")
  parseJSON _ = mzero

instance FromJSON Version where
  parseJSON (Object v) =
    liftA4 Version (toVersion <$> v .: "id")
                   (Just <$> (:[]) <$> v .: "link")
                   (toVersionStatus <$> v .: "status")
                   (VersionUpdated <$> v .: "updated")
  parseJSON _ = mzero

instance FromJSON VersionResponse where
  parseJSON (Object v) = VersionResponse <$> ((v .: "versions") >>= (.: "version"))
  parseJSON _ = mzero

instance FromJSON VersionDetailResponse where
  parseJSON (Object v) = do
    verId <- toVersion <$> ((v .: "version") >>= (.: "id"))
    links <- (v .: "version") >>= (.:? "links")
    status <- toVersionStatus <$> ((v .: "version") >>= (.: "status"))
    updated <- VersionUpdated <$> ((v .: "version") >>= (.: "updated"))
    return $ VersionDetailResponse (Version verId links status updated)
  parseJSON _ = mzero
