{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
module Main where

import Control.Applicative
import Data.Aeson
import Data.Char (chr)
import Data.Text
import Data.Time

import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Web.Rackspace.Identity.Common.Auth

instance Arbitrary Text where
  arbitrary = pack <$> listOf1 validChars
    where validChars = chr <$> choose (35, 126)

instance Arbitrary RoleDesc where
  arbitrary = RoleDesc <$> arbitrary

instance Arbitrary RoleId where
  arbitrary = RoleId <$> arbitrary

instance Arbitrary RoleName where
  arbitrary = RoleName <$> arbitrary

instance Arbitrary Role where
  arbitrary = liftA3 Role arbitrary arbitrary arbitrary

(?=) :: (Show a, Eq a) => Maybe a -> a -> Assertion
(?=) x y = case x of
  Nothing   -> False @? "Nothing"
  (Just x') -> x' @?= y

tests :: TestTree
tests = testGroup "Unit Tests"
  [ testCase "Decode Role" $
    decode "{\"description\": \"cat\", \"id\": \"1\", \"name\": \"2\"}" ?= roleExpected,

    testCase "Decode User" $
    decode "{\"RAX-AUTH:defaultRegion\": \"1\", \"id\": \"2\", \"name\": \"3\", \"roles\": []}" ?= userExpected,

    testCase "Decode ServiceEndpoint: All Fields Present" $
    decode "{\"publicURL\": \"1\", \"internalURL\": \"2\", \"region\": \"3\", \"tenantId\": \"4\"}" ?= endExpectedJJ,

    testCase "Decode ServiceEndpoint: Missing Internal URL" $
    decode "{\"publicURL\": \"1\", \"region\": \"3\", \"tenantId\": \"4\"}" ?= endExpectedNJ,

    testCase "Decode ServiceEndpoint: Missing Region" $
    decode "{\"publicURL\": \"1\", \"internalURL\": \"2\", \"tenantId\": \"4\"}" ?= endExpectedJN,

    testCase "Decode ServiceEndpoint: Missing InternalURL & Region" $
    decode "{\"publicURL\": \"1\", \"tenantId\": \"4\"}" ?= endExpectedNN,

    testCase "Decode Token" $
    decode "{\"expires\": \"2014-11-03T08:00:48Z\", \"id\": \"2\"}" ?= tokenExpected
  ]
  where roleExpected = Role (RoleDesc "cat") (RoleId "1") (RoleName "2")
        userExpected = User (Region "1") (TenantId "2") (Username "3") []
        endExpectedJJ = ServiceEndpoint (PublicUrl "1") (Just $ InternalUrl "2")
                                        (Just $ Region "3") (TenantId "4")
        endExpectedNJ = ServiceEndpoint (PublicUrl "1") Nothing
                                        (Just $ Region "3") (TenantId "4")
        endExpectedJN = ServiceEndpoint (PublicUrl "1") (Just $ InternalUrl "2")
                                        Nothing (TenantId "4")
        endExpectedNN = ServiceEndpoint (PublicUrl "1") Nothing
                                        Nothing (TenantId "4")
        tokenExpected = Token (Expires t) (TokenId "2")
        t = read "2014-11-03 08:00:48" :: UTCTime

tests' :: TestTree
tests' = testGroup "Property Tests"
  [ testProperty "Role: decode . encode == id"
    (\role -> (decode . encode) (role :: Role) == Just role)
  ]

main :: IO ()
main = defaultMain $ testGroup "Tests" [tests, tests']